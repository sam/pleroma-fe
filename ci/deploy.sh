#!/usr/bin/env bash
TARGET="pleroma@froth.zone:/opt/pleroma"

#rsync -ra public/ "${TARGET}/instance/static"
#cp dist/index.html  "${TARGET}/instance/static/index.html"
rsync --update --delete -Pr dist/ "${TARGET}/instance/static/frontends/pleroma-fe/froth"
# rsync --update -ra dist/static/ "${TARGET}/instance/static/frontends/pleroma-fe/froth"
#rsync --delete -ra images/ "${TARGET}/instance/static/images"
#rsync --delete -ra sounds/ "${TARGET}/instance/static/sounds"
rsync --update -ra instance/ "${TARGET}/instance/static/instance"
#rsync --delete -ra pages/ "${TARGET}/instance/static/pages"
