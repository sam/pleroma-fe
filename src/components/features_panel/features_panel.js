import fileSizeFormatService from '../../services/file_size_format/file_size_format.js'

const FeaturesPanel = {
  computed: {
    shout: function () { return this.$store.state.instance.shoutAvailable },
    pleromaChatMessages: function () { return this.$store.state.instance.pleromaChatMessagesAvailable },
    gopher: function () { return this.$store.state.instance.gopherAvailable },
    quotePosts: function () { return this.$store.state.instance.quotePostsAvailable },
    customReacts: function () { return this.$store.state.instance.pleromaCustomEmojiReactionsAvailable },
    whoToFollow: function () { return this.$store.state.instance.suggestionsEnabled },
    mediaProxy: function () { return this.$store.state.instance.mediaProxyAvailable },
    textlimit: function () { return this.$store.state.instance.textlimit },
    uploadlimit: function () { return fileSizeFormatService.fileSizeFormat(this.$store.state.instance.uploadlimit) }
  }
}

export default FeaturesPanel
